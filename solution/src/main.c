#include "../include/bmp_handler.h"
#include "../include/file_io.h"
#include "../include/image_handler.h"
#include "../include/rotate.h"

#include <stdio.h>

int main(int argc, char **argv) {
    if (argc != 3) {
        return 1;
    }

    FILE *input = NULL;
    FILE *output = NULL;

    char *input_file_name = argv[1];
    char *output_file_name = argv[2];

    if (!open_file(&input, input_file_name, "rb")) {
        return 1;
    }

    struct image img = {0};

    enum read_status status = from_bmp(input, &img);
    if (status != READ_OK) {
        destroy_image(&img);
        return 1;
    }

    if (!close_file(&input)) {
        destroy_image(&img);
        return 1;
    };

    struct image output_image = rotate(&img);
    destroy_image(&img);

    if (!open_file(&output, output_file_name, "wb")) {
        destroy_image(&output_image);
        return 1;
    }

    enum write_status write_status = to_bmp(output, &output_image);
    if (write_status != WRITE_OK) {
        destroy_image(&output_image);
        return 1;
    }

    if (!close_file(&output)) {
        destroy_image(&output_image);
        return 1;
    }

    destroy_image(&output_image);
    return 0;
}
